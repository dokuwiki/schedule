<?php
/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <dokuplugin@merciol.fr>
 *
 * Plugin Schedule: manage events per wiki @groups

 $df_ : 31/12/2000
 $ds_ : 20001231
 $dn_ : 978217200
 $dt_ : array ("Y" => 2000, "m" => 12, "d" => "31)
*/

if (!defined ('DOKU_INC'))
    define ('DOKU_INC', realpath (dirname (__FILE__).'/../../../').'/');
global $conf;
require_once (DOKU_INC.'inc/init.php');
if (!$conf['lang'])
    $conf['lang'] = 'fr';
require_once (DOKU_INC.'inc/common.php');
require_once (DOKU_INC.'inc/auth.php');

// ============================================================
if (isset ($_SERVER['REMOTE_USER']))
    $INFO['userinfo'] = $auth->getUserData ($auth->cleanUser ($_SERVER['REMOTE_USER']));
$schdplug =& plugin_load ('syntax', 'schedule_block');
$scheduleRoot = new scheduleRoot ($schdplug);
ob_start ();
switch ($_REQUEST ['schd']['action']) {
    // schedule Table 
case 'changeDate':
    global $conf;
    $dn_showDay = mktime (0, 0, 0, date ("n"), date ("j"), date("Y"));
    $ds_showDay = trim ($scheduleRoot->df2ds ($_REQUEST['schd']['date']));
    if (preg_match ("#(?<Y>[0-9][0-9][0-9][0-9])(?<m>[0-9][0-9])(?<d>[0-9][0-9])#", $ds_showDay, $dt_showDay))
        $dn_showDay = mktime (0, 0, 0, $dt_showDay["m"], $dt_showDay["d"], $dt_showDay["Y"]);
    $schedules = new schedules ($scheduleRoot, $_REQUEST['schd']['ns']);
    $schedules->load ();
    $schedules->printScheduleCalendar ($_REQUEST['schd']['mapId'], $dn_showDay);
    break;

    // schedule Root Admin
case 'moveSchedules':
    $scheduleRoot->manageAction ($_REQUEST['schd']);
    $scheduleRoot->printForm ();
    break;

default :
    ptnl ('No Ajax Function !');
}
$text = ob_get_contents ();
ob_end_clean ();
foreach ($scheduleRoot->message as $type => $msg)
    $text = '<div class="'.$type.'">'.$msg.'</div>'.$text;
ptln ($text);

// ============================================================
