<?php
/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <dokuplugin@merciol.fr>
 *
 * Plugin Schedule: manage events per wiki @groups
 */
if (!defined ('DOKU_INC'))
    define ('DOKU_INC', realpath (dirname (__FILE__).'/../../../').'/');
if (!defined ('DOKU_PLUGIN'))
    define ('DOKU_PLUGIN', DOKU_INC.'lib/plugins/');

// ============================================================
// event class
class schedule {
    var $id;			// event id
    var $member;		// event owner
    var $from;			// event begining date
    var $to;			// event ending date
    var $at;			// event begining hour
    var $what;			// event type
    var $where;			// event place insee code or city name
    var $lon;			// event place latitude
    var $lat;			// event place longitude
    var $addr;			// event place addresse
    var $audience;		// event expected audience
    var $shared;		// shared event
    var $title;			// event page title
    var $lead;			// event lead
    var $posterURL;		// event poster URL
    var $paperURL;		// event paper URL
    var $remark;		// event remark
    var $rate;			// event rate

    var $repeatFlag;	// event repeat or not
    var $repeat;		// event step of repeat
    var $repeatType;	// event type of repeat
    var $repeatGrouped;	// repeat event grouped on top
    var $weekDays;		// event array days concern by the repeating week 
    var $weekRank;		// event week concern by the repeating month 
    var $dayInWeek;		// event day in the week concern by the repeating month 
    var $dayRank;		// event day in the month concern by the repeating month 

    var $requestMembers;	// all members proposed during the request
}

// ============================================================
// sort event by event date, then creation date 
function cmpSchedule ($a, $b) {
    if ($a->from == $b->from) {
        if ($a->at == $b->at)
            return ($a->id < $b->id) ? -1 : 1;
        return ($a->at < $b->at) ? -1 : 1;
    }
    return ($a->from < $b->from) ? -1 : 1;
}
// sort info bubbles by date 
function cmpScheduleInDay ($a, $b) {
    if ($a['at'] == $b['at'])
        return ($a['id'] < $b['id']) ? -1 : 1;
    return ($a['at'] < $b['at']) ? -1 : 1;
}

// ============================================================
