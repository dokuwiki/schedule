<?php
/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <dokuplugin@merciol.fr>
 *
 * English language file
 */
 
// for the configuration manager
$lang['scheduleGroup']    = 'in this group one can add event';
$lang['adminGroup']       = 'admin group with all rights on events';
$lang['dataDir']          = 'directory contains XML file per group';
$lang['groupsDir']        = 'directory where group names could be find';
$lang['noSchedule']       = 'forbiden group names';
$lang['scheduleDir']      = 'wiki page for schedule (contains one dir per group)';
$lang['sharedDir']        = 'directory for shared events';
$lang['scheduleWhat']     = 'type of events per categrory (c1:w1,w2|c2:w3,w4|c3:w5,w6,w7)';
$lang['scheduleAudience'] = 'type of public';
$lang['iconName']         = 'logo name of the group which define an event (a1,a2,a3)';
$lang['iconWidth']        = 'logo width of the group which define an event';
$lang['repeatPosition']   = 'Repeat event position (grouped on top or isolated)';
$lang['bubblePosition']   = 'tooltips position of calendar';

$lang['useMap']        	  = 'Display map';
$lang['defaultCenter']    = "Empty map default localization (lat, lon)";
$lang['defaultZoom']      = "Default zoom";
$lang['migrate']          = 'Performe data migration';
$lang['showOldValues']	  = 'Highlight non migrate data';
?>
