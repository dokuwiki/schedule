<?php
/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <dokuplugin@merciol.fr>
 *
 * English language file
 */

// javascript
$lang['js']			=
    array ('pleaseWait' => 'Connection to the server in progress ...');

// commands
$lang['Valid']			= 'Valid';
$lang['add']			= 'Add';
$lang['clear']			= 'Clear cache';
$lang['clearAll']		= 'Clear all cache';
$lang['clearCache']		= 'Clear cache';	// XXX
$lang['created']		= 'Created';
$lang['modify']			= 'Modify';
$lang['modifyAll']		= 'Modify All';
$lang['prop']			= 'Suggest';
$lang['remove']			= 'Remove';
$lang['selected']		= 'Selected';
$lang['show']			= 'Show';
$lang['test']			= 'Test';

$lang['repeatType']		= array ('day' => 'days', 'week' => 'weeks', 'dayMonth' => 'months', 'dateMonth' => 'months', 'year' => 'years');

// placeholder
$lang['titlePH']		= 'event title';
$lang['leadPH']			= 'Lead paragraph / organisation team / context / ... (optional)';
$lang['posterPH']		= 'http://... (optional)';
$lang['paperPH']		= 'http://... (optional)';
$lang['remarkPH']		= 'explanation / comments / remarks / ... (optional)';
$lang['ratePH']			= 'x € / y € (optional)';
$lang['cityPH']			= 'city (or INSEE code) +!!enter!!';
$lang['addrPH']			= '1, rue de la Tannerie ~br~ 56 000 +!!enter!!';
$lang['datePH']			= 'mm/dd/aa';
$lang['hourPH']			= '--h-- (ex: 09h30-15h)';
$lang['rootMoveDatePH']		= 'mm/dd/aa (move until this date)';

// messages
$lang['days']			= array ('sunday' => 'S', 'monday' => 'M', 'tuesday' => 'T', 'wednesday' => 'W', 'friday' => 'F', 'thursday' => 'T', 'saturday' => 'S');
$lang['enterTip']		= 'Press \'enter\' to update this field!';

$lang['who']			= 'Who';
$lang['audience']		= 'Audience';
$lang['shared']			= array ('', 'Shared');
$lang['proposedBy']		= 'Proposed by';
$lang['what']			= 'What';
$lang['title']			= 'Title';
$lang['lead']			= 'Lead paragraph';
$lang['posterURL']		= 'Poster';
$lang['paperURL']		= 'Paper';
$lang['remark']			= 'Topic';
$lang['rate']			= 'Rate';
$lang['where']			= 'Where';
$lang['city']			= 'Commune';
$lang['addresse']		= 'Addresse';
$lang['when']			= 'When';
$lang['allDates']		= 'All dates';
$lang['inverted']		= 'Inverted';
$lang['noEvent']		= 'No event';
$lang['validation']		= 'Validation';
$lang['proposition']	= 'Proposition';
$lang['reactiveMap']	= 'Interactive map!';
$lang['toComplet']      = 'To complete';

$lang['audienceChoice']		= 'For whom ?';
$lang['memberChoice']		= 'Proposed by ?';
$lang['eventTypeChoice']	= 'What kind of event ?';

$lang['from']			= 'from';
$lang['to']			= 'to';
$lang['fromHour']		= 'from';
$lang['toHour']			= 'to';
$lang['at']			= 'at';
$lang['all']			= 'all';
$lang['each']			= 'each';

$lang['startError']		= "Validation couldn't be performed because:";
$lang['noAudienceError']	= 'audience miss';
$lang['noMemberError']		= 'member miss';
$lang['noTypeError']		= 'event type miss';
$lang['noWhereError']		= 'place miss';
$lang['noTitleError']		= 'title miss';
$lang['mktimeError']		= 'unsupported date (OVH server limitation is : 13 decembre 1901 < YYYY-mm-dd < 20 janvier 2038)';
$lang['pastError']		= 'date out of bounds';
$lang['pastToError']		= 'end date before start :-D';
$lang['badDateError']		= 'inappropriate date';
$lang['notMemberError']		= 'not member';

$lang['addEvent']		= 'Add an event';
$lang['proposedEvent']		= 'Suggest an event';

$lang['propSuccess']		= "Votre proposition est enregistrée. Nous allons vérifier qu'elle est conforme à l'esprit du FSL avant de la mettre en ligne. Merci de votre contributon.";

$lang['notifySubject']		= '[Schedule] New proposal !';
$lang['notifyContent']		= "A new proposal is done.\n\n";

// toolTip
$lang['tipPrevM']		= 'Previous mounth';
$lang['tipNextM']		= 'Next mounth';

// format
$lang['dateFormat']		= 'mdY';
// $lang['orderedFormat']	= function ($num) { // XXX pb OVH
//   if ($num < 1)
//     return $num;
//   switch ($num) {
//   case 1:
//     return '1st';
//   default:
//     return "{$num}th";
//   }
// };

$lang['orderedFormat']		= array (1 => '1st', 2 => '2th', 3 => '3th', 4 => '4th', 5 => '5th', 6 => '6th', 7 => '7th', 8 => '8th', 9 => '9th', 10 => '10th', 11 => '11th', 12 => '12th', 13 => '13th', 14 => '14th', 15 => '15th', 16 => '16th', 17 => '17th', 18 => '18th', 19 => '19th', 20 => '20th', 21 => '21th', 22 => '22th', 23 => '23th', 24 => '24th', 25 => '25th', 26 => '26th', 27 => '27th', 28 => '28th', 29 => '29th', 30 => '30th', 31 => '31th');

?>
