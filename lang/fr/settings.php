<?php
/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <dokuplugin@merciol.fr>
 *
 * French language file
 */

// for the configuration manager
$lang['scheduleGroup']             = 'en appartenant à ce groupe, on a accès au formulaire pour ajouter des évènements';
$lang['adminGroup']                = 'le groupe qui à tous les droits sur les évènements';
$lang['dataDir']                   = 'répertoire où sont placés les fichiers XML des évènements par entité';
$lang['groupsDir']                 = 'répertoire où se trouve les entités dans le wiki';
$lang['noSchedule']                = 'liste des entité (séparé par ",") ne devant pas créer des évènements';
$lang['scheduleDir']               = 'page wiki de gestion de l\'agenda (contient un répertoire par entité)';
$lang['sharedDir']                 = 'répertoire regroupant les évènements partagés par tous les groupes';
$lang['scheduleWhat']              = 'liste des types d\'évènement par catégorie (c1:t1,t2|c2:t3,t4|c3:t5,t6,t7)';
$lang['scheduleAudience']          = 'les différents auditoires attendus pour les évènement (a1,a2,a3)';
$lang['iconName']                  = 'nom du logo du groupe qui à créé l\'évènement';
$lang['iconWidth']                 = 'largeur logo du groupe qui à créé l\'évènement';
$lang['repeatPosition']            = 'Position des évènements répétés (groupé en haut ou séparé)';
$lang['repeatPosition_o_grouped']  = 'groupé';
$lang['repeatPosition_o_isolated'] = 'séparé';
$lang['bubblePosition']            = 'Position des info-bulles du calendrier';
$lang['bubblePosition_o_left']     = 'gauche';
$lang['bubblePosition_o_right']    = 'droite';

$lang['useMap']        	           = 'Affiche une carte de localisation';
$lang['defaultCenter']             = "Position par défaut d'une carte vide (lat, lon)";
$lang['defaultZoom']               = "Zoom par défaut";
$lang['migrate']                   = 'Effectue une migration des données';
$lang['showOldValues']	           = 'Souligne les données non migrées';
?>
