<?php
/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <dokuplugin@merciol.fr>
 *
 * French language file
 */

// javascript
$lang['js']			=
    array ('pleaseWait' => 'Connexion avec le serveur en cours ...');

// commands
$lang['add']			= 'Ajouter';
$lang['clear']			= 'Effacer le cache';
$lang['clearAll']		= 'Effacer tout les caches';
$lang['clearCache']		= 'Effacer le cache';	// XXX
$lang['created']		= 'Créer';
$lang['modify']			= 'Modifier';
$lang['modifyAll']		= 'Modifier Tout';
$lang['prop']			= 'Proposer';
$lang['remove']			= 'Supprimer';
$lang['selected']		= 'Selectionner';
$lang['show']			= 'Montrer';
$lang['test']			= 'Tester';
$lang['valid']			= 'Valider';

$lang['repeatType']		= array ('day' => 'jour', 'week' => 'semaine', 'dayMonth' => 'mois', 'dateMonth' => 'mois', 'year' => 'ans');

// placeholder
$lang['titlePH']		= "titre de l'évènement";
$lang['leadPH']			= 'Chapeau éditorial / organisateur / contexte / ... (facultatif)';
$lang['posterPH']		= 'http://... (facultatif)';
$lang['paperPH']		= 'http://... (facultatif)';
$lang['remarkPH']		= 'texte explicatif / commentaires / remarques / ... (facultatif)';
$lang['ratePH']			= 'x € / y € (facultatif)';
$lang['cityPH']			= 'commune (ou code INSEE) +!!entrée!!';
$lang['addrPH']			= '1, rue de la Tannerie ~br~ 56 000 +!!entrée!!';
$lang['datePH']			= 'jj/mm/aa';
$lang['hourPH']			= '--h-- (ex: 09h30-15h)';
$lang['rootMoveDatePH']		= "jj/mm/aa (déplace jusqu'à cette date)";

// messages
$lang['days']			= array ('dimanche' => 'D', 'lundi' => 'L', 'mardi' => 'M', 'mercredi' => 'M', 'jeudi' => 'J', 'vendredi' => 'V', 'samedi' => 'S');
$lang['enterTip']		= 'Appuyez sur \'entrée\' pour mettre à jour ce champ !';

$lang['who']			= 'Qui';
$lang['audience']		= 'Auditoire';
$lang['shared']			= array ('', 'Partagé');
$lang['proposedBy']		= 'Proposé par';
$lang['what']			= 'Quoi';
$lang['title']			= 'Titre ';
$lang['lead']			= 'Chapeau éditorial ';
$lang['posterURL']		= 'Affiche ';
$lang['paperURL']		= 'Article ';
$lang['remark']			= 'Sujet ';
$lang['rate']			= 'Tarif ';
$lang['where']			= 'Où ';
$lang['city']			= 'Commune ';
$lang['addresse']		= 'Adresse ';
$lang['when']			= 'Quand ';
$lang['allDates']		= 'Toutes dates';
$lang['inverted']		= 'Inverser';
$lang['noEvent']		= "Pas d'évènement";
$lang['validation']		= 'Validation';
$lang['proposition']	= 'Proposition';
$lang['reactiveMap']	= 'Carte interactive !';
$lang['toComplet']      = 'À compléter';

$lang['audienceChoice']		= 'Quelle audience ?';
$lang['memberChoice']		= 'Proposé par ?';
$lang['eventTypeChoice']	= "Quelle nature d'évènement ?";

$lang['from']			= 'du';
$lang['to']			= 'au';
$lang['fromHour']		= 'de';
$lang['toHour']			= 'à';
$lang['at']			= 'à';
$lang['all']			= 'chaque';
$lang['each']			= 'chaque';

$lang['startError']		= 'Certaines informations ne permettent pas la validation :';
$lang['noAudienceError']	= "manque d'auditoire";
$lang['noMemberError']		= 'manque de membre';
$lang['noTypeError']		= "manque de nature d'évènement";
$lang['noWhereError']		= 'manque de lieu';
$lang['noTitleError']		= 'manque de titre';
$lang['mktimeError']		= 'date non gérée (pour OVH : 13 decembre 1901 < YYYY-mm-dd < 20 janvier 2038)';
$lang['pastError']		= 'début en dehors des limites indiquées';
$lang['pastToError']		= 'date de fin avant le début :-D';
$lang['badDateError']		= 'date incorrecte';
$lang['notMemberError']		= 'non membre';

$lang['addEvent']		= 'Ajouter un évènement';
$lang['proposedEvent']		= 'Proposer un évènement';

$lang['propSuccess']		= "Votre proposition est enregistrée. Nous allons vérifier qu'elle est conforme à l'esprit du FSL avant de la mettre en ligne. Merci de votre contributon.";

$lang['notifySubject']		= '[Agenda] Nouvelle proposition !';
$lang['notifyContent']		= "Une nouvelle proposition a été faite.\n\n";

// toolTip
$lang['tipPrevM']		= 'Mois précédent';
$lang['tipNextM']		= 'Mois suivant';

// format
$lang['dateFormat']		= 'dmY';
// $lang['orderedFormat']	= function ($num) { // XXX pb OVH
//   if ($num < 1)
//     return $num;
//   switch ($num) {
//   case 1:
//     return '1er';
//   default:
//     return "{$num}e";
//   }
// };

$lang['orderedFormat']		= array (1 => '1er', 2 => '2e', 3 => '3e', 4 => '4e', 5 => '5e', 6 => '6e', 7 => '7e', 8 => '8e', 9 => '9e', 10 => '10e', 11 => '11e', 12 => '12e', 13 => '13e', 14 => '14e', 15 => '15e', 16 => '16e', 17 => '17e', 18 => '18e', 19 => '19e', 20 => '20e', 21 => '21e', 22 => '22e', 23 => '23e', 24 => '24e', 25 => '25e', 26 => '26e', 27 => '27e', 28 => '28e', 29 => '29e', 30 => '30e', 31 => '31e');

?>
