// ========================================
/* highlight day and cities in table according the event under the mouse */
function scheduleHighlightEvent (day, locations) {
    jQuery ('div.cityBubble div.bubble').each (function () {
	jQuery (this).hide ();
    });
    scheduleHighlightPOI (locations);
    jQuery ('div.schedule table.short div.mapSelect').each (function () {
	var showDay = jQuery (this);
	var today = showDay.attr ('day');
	if (day && today == day)
	    showDay.show ();
	else
	    showDay.hide ();
    });
}

// ========================================
/* highlight all days in table according the city under the mouse */
function scheduleHighlightDays (targetLocations) {
    jQuery ('div.cityBubble div.bubble').each (function () {
	jQuery (this).hide ();
    });
    if (!targetLocations || !targetLocations.length) {
	jQuery ('div.schedule table.short div.mapSelect').hide ();
	return;
    }
    targetLocations.forEach (function (location) {
	jQuery ('div.cityBubble[location="'+location+'"] div.bubble').each (function () {
	    jQuery (this).show ();
	});
    });
    jQuery ('div.schedule table.short div.mapSelect').each (function () {
	var showDay = jQuery (this);
	var eventLocations = showDay.attr ('locations');
	if (!eventLocations) {
	    showDay.hide ();
	    return;
	}
	var doShow = false;
	targetLocations.forEach (function (location) {
	    if ((','+eventLocations+',').indexOf (location) >= 0) {
		doShow = true;
		return;
	    }
	});
	if (doShow)
	    showDay.show ();
	else
	    showDay.hide ();
    });
}

var scheduleMapList;

// ========================================
/* highlight all cities on the map according the day under the mouse */
function scheduleHighlightPOI (locations) {
    if (!locations) {
	jQuery ('div.schedule table.short div.mapSelect').hide ();
	jQuery.each (scheduleMapList, function (mapId, scheduleMap) {
	    scheduleMap.poi.forEachFeature (function (item) {
		item.set ('selected', false);
	    });
	    scheduleMap.poi.changed ();
	    scheduleMap.map.render ();
	});
	return;
    }
    jQuery.each (scheduleMapList, function (mapId, scheduleMap) {
	var changed = false;
	scheduleMap.poi.forEachFeature (function (item) {
	    var location = item.get ('location');
	    item.set ('selected', (location && (','+locations).indexOf (location) >= 0));
	});
	scheduleMap.poi.changed ();
	scheduleMap.map.render ();
    });
}
// ========================================
function scheduleChangeDate (obj, ns, mapId, date) {
    var table = jQuery (obj).closest ("table")[0];
    scheduleSend (table,
		  DOKU_BASE+"lib/plugins/schedule/ajax.php",
		  "schd[ns]="+ns+"&schd[mapId]="+mapId+"&schd[action]=changeDate&schd[date]="+date);
}

// ========================================
function scheduleSelectDate (date) {
    jQuery ("#scheduleFrom").val (date);
}

// ========================================
