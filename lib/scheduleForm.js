// ========================================

var scheduleDoCkeck = false;

function scheduleForceCheckInputs () {
    scheduleDoCkeck = true;
    return scheduleCheckInputs ();
}

function scheduleCheckSelect (select) {
    var warning = select.find ('option:selected').val () == "";
    select.prev (".warningPlace").toggleClass ("warning", warning);
    return warning;
}
function scheduleCheckInput (input) {
    var warning = input.val () == "";
    input.toggleClass ("warning", warning);
    return warning;
}
function scheduleCheckUL (ul) {
    var warning = ul.children ().length == 0;
    ul.toggleClass ("warning", warning);
    return warning;
}
/*
 * highlight missing input fields
 */
function scheduleCheckInputs () {
    if (!scheduleDoCkeck)
	return false;
    var tab = jQuery (".scheduleTabForm");
    var tabs = tab.find ("li a.warningPlace");
    var checked = true;
    var warning = false;
    warning = scheduleCheckSelect (tab.find ('select[name="schd[audience]"]'));
    warning = scheduleCheckSelect (tab.find ('select.members')) || warning;
    tabs.slice (0,1).toggleClass ("warning", warning);
    if (warning)
	checked = false;
    warning = scheduleCheckSelect (tab.find ('select[name="schd[what]"]'));
    warning = scheduleCheckInput (tab.find ('input[name="schd[title]"]')) || warning;
    tabs.slice (1,2).toggleClass ("warning", warning);
    if (warning)
	checked = false;
    warning = scheduleCheckUL (tab.find ('ul.cities'));
    tabs.slice (2,3).toggleClass ("warning", warning);
    if (warning)
	checked = false;
    warning = scheduleCheckInput (tab.find ('input[name="schd[from]"]'));
    tabs.slice (3,4).toggleClass ("warning", warning);
    if (warning)
	checked = false;
    warning = scheduleCheckInput (tab.find ('input.edit'));
    tabs.slice (4,5).toggleClass ("warning", warning);
    if (warning)
	checked = false;
    return checked;
}

/**
 * switch all selected events for delete.
 */
function scheduleSwitchSelection (form) {
    for (var i = 0; i < form.elements.length; i++)
	form.elements[i].checked = !form.elements[i].checked;
}

/*
 * change member selection mode according share status
 */
function scheduleSharedEvent (form) {
    var form = jQuery (form);
    var shared = null;

    form.find ('select[name="schd[shared]"]').each (function () {
	if (this.options [this.selectedIndex].text != "")
	    shared = "true";
    });
    form.find ('select[name="schd[member]"]').each (function () {
	this.multiple = shared;
	this.size = shared ? 9 : 1;
    });
}

/*
 * display repeat input according selection
 */
function scheduleUpdateRepeatType (repeatSelection) {
    var selectedValue = repeatSelection.options [repeatSelection.selectedIndex].value;
    var inputs = repeatSelection.form.elements;
    var disabledWeek = (selectedValue != "week");
    var disabledDay  = (selectedValue != "dayMonth");
    var disabledDate = (selectedValue != "dateMonth");
    var checkedDay = 0;
    // XXX jquery ?
    for (var i = 0; i < inputs.length; i++) {
	var name = inputs[i].name;
	if (name == "schd[weekDays][]") {
	    inputs[i].disabled = disabledWeek;
	    if (inputs[i].checked)
		checkedDay++;
	} else if (name == "schd[weekRank]")
	    inputs[i].disabled = disabledDay;
	else if (name == "schd[dayInWeek]")
	    inputs[i].disabled = disabledDay;
	else if (name == "schd[dayRank]")
	    inputs[i].disabled = disabledDate;
    }
    if (!disabledWeek && checkedDay == 0)
	scheduleForceDay (inputs);
    // XXX forcer le Ie jour du mois
}

/*
 * display repeat input according selection
 */
function scheduleUpdateWeekDays (DaySelection) {
    var inputs = DaySelection.form.elements;
    var checkedDay = 0;
    // XXX jquery
    for (var i = 0; i < inputs.length; i++) {
	if (inputs[i].name == "schd[weekDays][]")
	    if (inputs[i].checked)
		checkedDay++;
    }
    if (checkedDay == 0)
	scheduleForceDay (inputs);
}

function scheduleForceDay (inputs) {
    var date = document.getElementById ("scheduleFrom").value;
    if (!date)
	date = new Date ();
    else {
	date = date.split ('/'); // XXX bug format FR seulement
	date [1] = parseInt (date [1]) - 1;
	if (date[2] < 100)
	    date [2] = parseInt (date [2]) + 2000;
	date = new Date (date[2], date [1], date [0]);
    }
    var rankDay = date.getDay();
    for (var i = 0; i < inputs.length; i++)
	if (inputs[i].name == "schd[weekDays][]" && inputs[i].value == rankDay)
	    inputs[i].checked = true;
}
