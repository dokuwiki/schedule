<?php
/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <dokuplugin@merciol.fr>
 *
 * Plugin Schedule: manage events per wiki @groups

 */

if (!defined ('DOKU_INC'))
    define ('DOKU_INC', realpath (dirname (__FILE__).'/../../../').'/');
require_once (DOKU_INC.'inc/init.php');
require_once (DOKU_INC.'inc/common.php');
require_once (DOKU_INC.'inc/auth.php');

// ============================================================
if (isset ($_SERVER['REMOTE_USER']))
    $INFO['userinfo'] = $auth->getUserData ($auth->cleanUser ($_SERVER['REMOTE_USER']));
$schdplug =& plugin_load ('syntax', 'schedule_block');
$scheduleRoot = new scheduleRoot ($schdplug);

switch ($_REQUEST ['schedule']['action']) {
case 'clear':
    $schedules = new schedules ($scheduleRoot, $_REQUEST ['schedule']['ns']);
    $schedules->clearCache ();
    break;
case 'clearAll':
    $scheduleRoot->clearCache ();
    break;
}

// ============================================================
