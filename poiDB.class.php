<?php
/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <dokuplugin@merciol.fr>
 *
 * Plugin Schedule: manage events per wiki @groups

 */

// ============================================================
class poiDB {
    var $modify = false;
    var $dbFileName;
    var $dbByInsee;
    var $allLines;

    // ============================================================
    function __construct ($plugin) {
        global $conf;
        $this->dbFileName =
                          ((!$conf['savedir'] || strpos ($conf['savedir'], '.') === 0) ? DOKU_INC : "").
                          $conf['savedir'].'/'.trim ($plugin->getConf ('dataDir').'/poiDB.txt');
        $this->read ();
    }

    // ============================================================
    function getInsee ($insee) {
        if (!isset ($this->dbByInsee [$insee]))
            return array ();
        return $this->dbByInsee [$insee];
    }

    // ============================================================
    function addLine ($line) {
        $this->cleanDB ();
        $line = $this->cleanLine ($line);
        if (!$this->allLines || !in_array ($line, $this->allLines)) {
            $this->allLines [] = $line;
            sort ($this->allLines);
            $this->modify = true;
        }
        $this->write ();
        echo "record".NL;
    }

    // ============================================================
    function removeLine ($line) {
        $this->cleanDB ();
        $line = $this->cleanLine ($line);

        if (($key = array_search ($line, $this->allLines)) !== false) {
            unset ($this->allLines [$key]);
            $this->modify = true;
        }
        $this->write ();
    }

    // ============================================================
    function cleanLine ($line) {
        // XXX vérifier nombre
        $data = explode ("|", $line);
        foreach ($data as $key => $val)
            $data[$key] = trim ($val);
        $data [1] = number_format ((float)$data[1], 4);
        $data [2] = number_format ((float)$data[2], 4);
        return implode ("|", $data);
    }

    // ============================================================
    function cleanDB () {
        if ($this->allLines)
            foreach ($this->allLines as $key => $line) {
                $lineCleaned = $this->cleanLine ($line);
                if ($lineCleaned != $line)
                    $this->modify = true;
                $this->allLines [$key] = $lineCleaned;
            }
    }

    // ============================================================
    function read () {
        $this->dbByInsee = array ();
        $handle = @fopen ($this->dbFileName, "r");
        if ($handle) {
            while (($line = fgets ($handle)) !== false) {
                $line = trim (preg_replace ("/#.*$/", "", str_replace ('\\\\', '~br~', $line)));
                if (!$line)
                    continue;
                $this->allLines [] = $line;
                list ($insee, $end) = explode ('|', $line, 2);
                $this->dbByInsee [$insee][] = $end;
            }
            fclose ($handle);
        }
    }

    // ============================================================
    function write () {
        if (!$this->modify) {
            echo "POI DB unchanged !".NL;
            return;
        }
        $handle = @fopen ($this->dbFileName, "w");
        if ($handle == false)
            die("unable to create file: ".$this->dbFileName); // XXX
        foreach ($this->allLines as $line)
            fputs ($handle, $line.NL);
        fclose ($handle);
        echo "POI DB updated !".NL;
    }

    // ============================================================
}
