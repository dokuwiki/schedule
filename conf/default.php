<?php
/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <dokuplugin@merciol.fr>
 *
 * Options for the Schedule Plugin
 */
$conf['scheduleGroup']    = 'schedule';
$conf['adminGroup']       = 'admin';
$conf['dataDir']          = 'schedule';
$conf['groupsDir']        = 'membres';
$conf['noSchedule']       = '2010';
$conf['scheduleDir']      = 'agenda';
$conf['sharedDir']        = 'commun';
$conf['scheduleWhat']     = 'A:Manifestation, Vote, Pétition, Action|B:Echange, Forum, Débat, Conférence, Projection, Exposition, Spectacle, Radio, TV, Pique-Nique|C:Assemblée Générale, Réunion, Mensuelle, Hebdomadaire';
$conf['scheduleAudience'] = 'Ouvert à tous, Membres, Commission, Bureau';
$conf['iconName']         = 'logo.png';
$conf['iconWidth']        = 30;
$conf['repeatPosition']   = 'isolated';
$conf['bubblePosition']   = 'right';

$conf['useMap']        	  = 1;
$conf['defaultCenter']    = "[47.95583, -2.54833]";
$conf['defaultZoom']      = 14;
$conf['migrate']          = 0;
$conf['showOldValues']    = 0;
?>
