<?php
/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <dokuplugin@merciol.fr>
 *
 * Metadata for configuration manager plugin
 * Additions for the schedule plugin
 */
$meta['scheduleGroup']    = array('string');	// who can use form
$meta['adminGroup']       = array('string');	// who can admin data
$meta['dataDir']          = array('string');	// root dir for XML files
$meta['groupsDir']        = array('string');	// dir group in wiki pages to find group names
$meta['noSchedule']       = array('string');	// forbiden name for group (ex skip annual archive dir)
$meta['scheduleDir']      = array('string');	// dir in wiki pages for events (contains subdir for group name or the shared dir)
$meta['sharedDir']        = array('string');	// dir for shared event in wiki pages
$meta['scheduleWhat']     = array('string');	// type event list
$meta['scheduleAudience'] = array('string');	// type audience list
$meta['iconName']         = array('string');	// file name containing logo in group dir 
$meta['iconWidth']        = array('numeric');	// default logo width size
$meta['repeatPosition']   = array('multichoice', '_choices' => array('grouped', 'isolated'));	// does the repeated events are group on top of form
$meta['bubblePosition']   = array('multichoice', '_choices' => array('left', 'right'));			// tooltips position

$meta['useMap']        	  = array('onoff');
$meta['defaultCenter']    = array('string');
$meta['defaultZoom']      = array('numeric');
$meta['migrate']          = array('onoff');		// XXX to remove ?
$meta['showOldValues']    = array('onoff');		// XXX to remove ?
?>
