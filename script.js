/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <dokuplugin@merciol.fr>
 *
 * Javascript functionality for the include plugin
 */

/* DOKUWIKI:include_once lib/scheduleForm.js */
/* DOKUWIKI:include_once lib/scheduleTable.js */
/* DOKUWIKI:include_once ../ol3/script.js */
/* DOKUWIKI:include_once lib/scheduleCitiesMap.js */

// ========================================
// Ajax function
// ========================================
/* Clear popup message */
function scheduleClearMsgJ (item) {
    item.closest ("div").find ("div.popup").each (function () {
	jQuery (this).remove ();
    });
}

function scheduleClearCache (action, ns) {
    jQuery.ajax ({
	type: "POST",
	url: DOKU_BASE+"lib/plugins/schedule/ajaxClearCache.php",
	//success: function (response) { alert (response); },
	cache: true,
	async: true,
	data: "schedule[action]="+action+"&schedule[ns]="+ns
    });
}

/* performe ajax request to swap month */
function scheduleAjax (form, action, md5) {
    var params = "";
    // XXX jquery
    for (var idx = 0; idx < form.elements.length; idx++) {
	var elem = form.elements[idx];
	if (elem.type == "checkbox") {
	    if (elem.checked)
		params += "&"+elem.name+"="+elem.value;
	} else if (elem.type == "select" || elem.type == "select-multiple") {
	    while (elem.options.selectedIndex >= 0) {
		opt = elem.options [elem.options.selectedIndex];
		params += "&"+elem.name+"="+opt.text;
		elem.options [elem.options.selectedIndex] = null;
	    }
	} else
	    params += "&"+elem.name+"="+elem.value;
    }
    params += "&schd[action]="+action+"&schd[md5]="+md5;

    scheduleSend (form,
		  DOKU_BASE+"lib/plugins/schedule/ajax.php",
		  params);
    return false;
}

function scheduleSend (sender, uri, params) {
    var jDiv = jQuery (sender).closest ("div");
    scheduleClearMsgJ (jDiv);
    jQuery ('<div class="popup">'+LANG["plugins"]["schedule"]["pleaseWait"]+'</div>').
	insertBefore (jDiv.children ().first ());
    jQuery.ajax ({
	type: "POST",
	url: uri,
	cache: false,
	async: true,
	data: params,
	success: function (response) {
	    jDiv.html (response);
	    scheduleInitMaps ();
	    scheduleInitPOI ();
	}
    });
}

// ========================================
