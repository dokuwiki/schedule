<?php
/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <dokuplugin@merciol.fr>
 *
 * Plugin Schedule: manage events per wiki @groups

 */

// faire un XML

if (!defined ('DOKU_INC'))
    define ('DOKU_INC', realpath (dirname (__FILE__).'/../../../').'/');
require_once (DOKU_INC.'inc/init.php');
require_once (DOKU_INC.'inc/common.php');
require_once (DOKU_INC.'inc/auth.php');
require_once (realpath (dirname (__FILE__)).'/poiDB.class.php');

// ============================================================
if (isset ($_SERVER['REMOTE_USER']))
    $INFO['userinfo'] = $auth->getUserData ($auth->cleanUser ($_SERVER['REMOTE_USER']));
$schdplug =& plugin_load ('syntax', 'schedule_block');
$isAdmin =
         isset ($INFO ['userinfo']) &&
         isset ($INFO ['userinfo']['grps']) &&
         in_array (trim ($schdplug->getConf ('adminGroup')), $INFO ['userinfo']['grps']);

$poiDB = new poiDB ($schdplug);
if ('list' == $_REQUEST ['action'])
    echo json_encode ($poiDB->getInsee ($_REQUEST['insee']));
elseif ($isAdmin) {
    switch ($_REQUEST ['action']) {
    case 'add':
        $poiDB->addLine ($_REQUEST ['line']);
        break;
    case 'remove':
        $poiDB->removeLine ($_REQUEST ['line']);
        break;
    default:
        die ('No Ajax Function !');
    }
} else
      die ("No Ajax Function !");

// ============================================================
